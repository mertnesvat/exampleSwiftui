//
//  ContentView.swift
//  exampleBerk
//
//  Created by Mert Neşvat on 5/17/20.
//  Copyright © 2020 Mert Nesvat. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State var name: String = ""
    @State var formExampleShouldShow: Bool = false
    @State var showHello: Bool = true
    var body: some View {
        NavigationView {
            VStack {
                if showHello {
                    Text("4")
                        .font(.title)
                        .fontWeight(.ultraLight)
                        .foregroundColor(Color.black)
                        .padding()
                }
                TextField("your name", text: $name).multilineTextAlignment(.center)
                Divider()
                NavigationLink(destination: FormExample(), isActive: $formExampleShouldShow) {
                    Text("This is heart button!")
                    Image(systemName: "heart.fill")
                }
                Button(action: {
                    self.showHello.toggle()
                }) {
                    ZStack {
                        Rectangle().foregroundColor(.red)
                        Text("I'm here already!").foregroundColor(.purple)
                    }
                }.buttonStyle(PlainButtonStyle())
            }.padding([.leading, .trailing], 80)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
