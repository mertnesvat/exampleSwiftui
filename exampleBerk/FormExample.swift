//
//  FormExample.swift
//  exampleBerk
//
//  Created by Mert Neşvat on 5/17/20.
//  Copyright © 2020 Mert Nesvat. All rights reserved.
//

import SwiftUI

struct FormExample: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct FormExample_Previews: PreviewProvider {
    static var previews: some View {
        FormExample()
    }
}
